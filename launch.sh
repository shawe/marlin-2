#!/bin/bash

DATE=$(date +'%Y-%m-%d')
BASE_FOLDER="config/examples/Creality/Ender-3/"

## Limpiamos compilaciones previas de hoy
for BOARD in "${BASE_FOLDER}*"; do
    for FOLDER in ${BOARD}/*; do
        if [[ -d "$FOLDER" ]]; then
            echo -e "- Limpiando ${FOLDER} ..."
            FIRMWARE_FOLDER="${FOLDER}/firmware/${DATE}/"
            rm -rf "${FIRMWARE_FOLDER}"

            if [[ -f "${FOLDER}/not-autocompile" ]]; then
                echo -e "   - Ignorando creación de carpeta ${FOLDER}"
                continue
            fi

            mkdir -p "${FIRMWARE_FOLDER}"
        fi
    done
done

for BOARD in "${BASE_FOLDER}*"; do
    for FOLDER in ${BOARD}/*; do
        if [[ -d "$FOLDER" ]]; then
            echo -e " - Compilación ${FOLDER}"

            if [[ -f "${FOLDER}/not-autocompile" ]]; then
                echo -e "   - Ignorando compilación para ${FOLDER}"
                continue
            fi

            FIRMWARE_FOLDER="${FOLDER}/firmware/${DATE}/"
            if [[ -f "${FIRMWARE_FOLDER}Robin_e3.bin" ]]; then
                echo -e "   - Ya existe ${FIRMWARE_FOLDER}"
            else
                echo -e " - Copiando platformio..."
                cp -R "${FOLDER}"/platformio.ini "platformio.ini"

                echo -e " - Preparando ${FOLDER} ..."
                cp -R "${FOLDER}"/*.h Marlin/
                echo -e " - Limpiando entorno ..."
                #python buildroot/share/vscode/auto_build.py clean
                platformio run -t clean

                echo -e " - Compilando entorno ${FOLDER} ..."
                #python buildroot/share/vscode/auto_build.py build
                sleep 1
                platformio run

                if [[ -f ".pio/build/mks_robin_e3/Robin_e3.bin" ]]; then
                    cp .pio/build/mks_robin_e3/Robin_e3.bin "${FIRMWARE_FOLDER}"
                    echo -e " - Imagen copiada en ${FIRMWARE_FOLDER}"
                    sleep 3
                elif [[ -f ".pio/build/STM32F103RC_btt_512K_USB/firmware.bin" ]]; then
                    cp .pio/build/STM32F103RC_btt_512K_USB/firmware.bin "${FIRMWARE_FOLDER}"
                    echo -e " - Imagen copiada en ${FIRMWARE_FOLDER}"
                    sleep 3
                else
                    echo -e " - Fallo en ${FOLDER}"
                    sleep 10
                fi
            fi
        fi
    done
done

yes | cp config/examples/base/*.h Marlin/
rm -rf Marlin/_*.h
yes | cp config/examples/base/platformio.ini .
