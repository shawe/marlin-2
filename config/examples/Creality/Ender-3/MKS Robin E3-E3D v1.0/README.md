# Configuraciones y firmware pre-compilados

A continuación tienes disponibles las configuraciones y firmwares pre-compilados de Marlin2 en rama 2.0.x.

Puedes utilizar las configuraciones y hacer tus propias variaciones o directamente descargar el firmware y flashearlo en tu impresora.

Si te resulta de utilidad, puedes hacer una pequeña donación con [![PayPal](https://www.paypalobjects.com/webstatic/mktg/logo/pp_cc_mark_37x23.jpg)](https://www.paypal.me/FrancescPineda)


## Donadores

Muchas gracias por las aportaciones a:

- @Credenze
- @jespitec
- @anmo1


## Enlaces oficiales:

- [MKS Robin E3 V1.0 User Manual](https://github.com/makerbase-mks/MKS-Robin-E3-E3D/wiki/MKS-Robin-E3-V1.0-User-Manual)
- [MKS Robin E3D V1.0 User Manual](https://github.com/makerbase-mks/MKS-Robin-E3-E3D/wiki/MKS-Robin-E3D-V1.0-User-Manual)


## Como flashear el firmware?

- Copiar el archivo Robin_e3.bin a tu SD.
- Inserta la SD en la placa MKS Robin E3/E3D.
- Reinicia o enciende la impresora.


## Es necesario hacer algo adicional tras el flasheo?

- Se recomienda siempre Inicializar la EEPROM
- Ajustar pasos del extrusor si fuera necesario
- Ajustar PIDs
  - PID Extrusor (Ejemplo a 210, cambiar por la temperatura de uso habitual):
    ```
      M106 S128; Encendemos ventilador de capa (255 = 100%, ajustar al valor de uso real)
      M303 E0 S210 C8 U1; Iniciamos el proceso para el extrusor y actualizamos valores
      M500; Guardamos los valores
      M106 S0; Paramos ventilador de capa
    ```
  - PID Cama (Ejemplo a 60, cambiar por la temperatura de uso habitual):
    ```
      M106 S128; Encendemos ventilador de capa (255 = 100%, ajustar al valor de uso)
      M303 E-1 S60 C8 U1; Iniciamos el proceso para la cama y actualizamos valores
      M500; Guardamos los valores
      M106 S0; Paramos ventilador de capa
    ```

## Sensorless homing

En principio para activar el Sensorless homing es suficiente con poner los jumpers como se indica en [Sensorless_homing with tmc2209](https://github.com/makerbase-mks/MKS-Robin-E3-E3D/wiki/Sensorless_homing-with-tmc2209) y flashear una de las variantes con *Sensorless homing* activado.

A mi personalmente, me da la impresión de que esta característica no funciona como debe.

Se ha contrastado con el [Manual para la SKR](https://drive.google.com/file/d/1SmzHyG6Dl_GE3H9HM7tW8fV8YcZxMSde/view?hl=en), concretamente las páginas 37 y 38, pero tampoco parece conseguirse ninguna mejora.

En mi caso, sigue haciendo el homing a través de los endstop, ya que si interrumpo el desplazamiento, no lo considera como una colisión y que haya llegado al final.


## Como conectar un sensor de proximidad BLTouch/3DTouch

### Conexiones a placa

![Conexiones a placa](https://gitlab.com/shawe/marlin-2/-/raw/2.0.x/config/examples/Creality/Ender-3/MKS%20Robin%20E3-E3D/Connect_MKS_Robin_E3D.png)

**Muy importante** asegurarse que está todo bien conectado antes de encender la impresora, de no ser así puede dañarse el BLTouch/3DTouch y no tener arreglo.

### Cambios importantes a tener en cuenta

Cada mod para el hotend que puede imprimirse tiene un offset diferente, y esto influye en que hay que indicar correctamente dichos valores para evitar dañar el BLTouch/3DTouch.

Si tienes que modificar estos valores, debes indicarlo en el código buscando NOZZLE_TO_PROBE_OFFSET y rellenando los valores como se indica.

### Vídeo oficial de MakerBase

[3D Touch bltouch tutorial with LCD12864 and MKS Robin E3D SGen_L Gen_L](https://www.youtube.com/watch?v=r2A0b7XQEaI)


## Como conectar un sensor de proximidad inductivo

Antes de conectar nada, cosas importantes a tener en cuenta:

- **Requiere un fleje** para que el sensor inductivo detecte la cama
- Es NPN o PNP
- Es NC o NO

El sensor de proximidad inductivo utilizado para esta explicación es este [Sensor de proximidad inductivo LJ8A3-2-Z/BX Trianglelab M8 DC5V](https://es.aliexpress.com/item/33006152423.html). Es de **5V, tipo NPN y NO**, con **referencia LJ8A3-2-Z/BX**.

![Sensor de proximidad inductivo LJ8A3-2-Z/BX](https://gitlab.com/shawe/marlin-2/-/raw/2.0.x/config/examples/Creality/Ender-3/MKS%20Robin%20E3-E3D/LJ8A3-2-Z-BX.jpeg)

Si tienes otro tipo, **asegurate como debe conectarse antes de hacer nada**, si se conecta mal puede dañar el sensor y/o la placa.

Otras alternativas:
- [Sensor inductivo de proximidad 6~36V NPN PNP 90~250VAC NO+NC M18 M24 detectar Metal interruptor Sensor de 4mm, 8mm, 10mm](https://es.aliexpress.com/item/4000324890146.html), requiere un octoacoplador si va a conectarse a 24V.


### Utilizando el E-STOP de Z (método recomendado)

El sensor de proximidad inductivo utilizado para esta explicación es este [Sensor de proximidad inductivo LJ8A3-2-Z/BX Trianglelab M8 DC5V](https://es.aliexpress.com/item/33006152423.html). Es de **5V, tipo NPN y NO**, con **referencia LJ8A3-2-Z/BX**.

![Sensor de proximidad inductivo LJ8A3-2-Z/BX](https://gitlab.com/shawe/marlin-2/-/raw/2.0.x/config/examples/Creality/Ender-3/MKS%20Robin%20E3-E3D/LJ8A3-2-Z-BX.jpeg)

Si tienes otro tipo, **asegurate como debe conectarse antes de hacer nada**, si se conecta mal puede dañar el sensor y/o la placa.

![Conexiones del inductivo al endstop de Z](https://gitlab.com/shawe/marlin-2/-/raw/2.0.x/config/examples/Creality/Ender-3/MKS%20Robin%20E3-E3D/MKS_Robin_E3_inductivo_ENDSTOP.png)

Una vez conectado, podemos probar con el comando M119 por Pronterface para ver el estado del sensor, y en función del resultado, ver si necesitamos o no invertirlo en el código.

Ejecutamos sin tener nada metálico cerca del sensor:
```
> M119
Reporting endstop status
x_min: open
y_min: open
z_min: TRIGGERED
z_probe: open
```

Repetiremos el procedimiento con un fleje:
```
> M119
Reporting endstop status
x_min: open
y_min: open
z_min: open
z_probe: open
```

Debería estar en **open**, por tanto necesitamos invertir el endstop. Si hubiera aparecido en open, no sería conveniente hacer nada más.

Buscaremos en Configuration.h estas constantes, y las pondremos a true.

```
#define Z_MIN_ENDSTOP_INVERTING false // Set to true to invert the logic of the endstop.
#define Z_MIN_PROBE_ENDSTOP_INVERTING false // Set to true to invert the logic of the probe.
```


### Utilizando el PROBE (método alternativo)

**WIP: Por probar que funcione bien.**

El sensor de proximidad inductivo utilizado para esta explicación es este [Sensor de proximidad inductivo LJ8A3-2-Z/BX Trianglelab M8 DC5V](https://es.aliexpress.com/item/33006152423.html). Es de **5V, tipo NPN y NO**, con **referencia LJ8A3-2-Z/BX**.

![Sensor de proximidad inductivo LJ8A3-2-Z/BX](https://gitlab.com/shawe/marlin-2/-/raw/2.0.x/config/examples/Creality/Ender-3/MKS%20Robin%20E3-E3D/LJ8A3-2-Z-BX.jpeg)

Si tienes otro tipo, **asegurate como debe conectarse antes de hacer nada**, si se conecta mal puede dañar el sensor y/o la placa.

![Conexiones del inductivo al endstop de Z](https://gitlab.com/shawe/marlin-2/-/raw/2.0.x/config/examples/Creality/Ender-3/MKS%20Robin%20E3-E3D/MKS_Robin_E3_inductivo_PROBE.png)


## Como compilar Marlin 2?

Como al fin y al cabo todos tenemos configuraciones diferentes pero a la vez muy parecidas, aunque partamos de una configuración base común, se indican los pasos para agilizar la parte común y sólo tener que modificar las particularidades a cambiar si:

- [Descargar Visual Studio Code](https://code.visualstudio.com/)
- Instalar la extensión *PlatformIO*
- Instalar la extensión *Auto Build Marlin*
- [Descargar Marlin 2 2.0.x](https://gitlab.com/shawe/marlin-2/-/archive/2.0.x/marlin-2-2.0.x.zip)
- Descomprimir el Marlin descargado en el punto anterior
- Desde *Visual Studio Code* en la pestaña *PIO Home*, pulsar en *Open Project* y buscar la carpeta que se ha descomprimido en el paso anterior.
- En la carpeta *config/examples/Creality/Ender-3/MKS%20Robin%20E3-E3D/* hay una serie de configuraciones preparadas, utilizar una de estas a modo de configuración para ello
  - Hay que *copiar los archivos *.h del contenido de una de estas carpetas* (la que sea más parecida a tu montaje actual) y *pegarla en la carpeta Marlin*
  - Hay que *copiar el archivo platformio.ini del contenido de una de estas carpetas* y *pegarlo en la carpeta raíz (un nivel antes de Marlin)*
- Ya puedes abrir *Auto Build Marlin* y pulsar en *Build*
- Una vez finalizada la compilación, el firmare está en *.pio/build/mks_robin_e3/Robin_e3.bin*, copialo y pegalo en tu SD y reinicia la impresora.
